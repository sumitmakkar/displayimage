#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>

#include<iostream>
#include<conio.h>
using namespace cv;

int main(int argc, char** argv) 
{
	IplImage* img = cvLoadImage("image.jpg");
	cvNamedWindow("Sachin Tendulkar", CV_WINDOW_AUTOSIZE);
	cvShowImage("Sachin Tendulkar", img);
	cvWaitKey(0);
	cvReleaseImage(&img);
	cvDestroyWindow("Sachin Tendulkar");
	return 0;
}